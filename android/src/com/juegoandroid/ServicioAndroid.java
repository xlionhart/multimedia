package com.juegoandroid;

import android.app.Service;
import android.content.Intent;

import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;


//Clase del Servicio que te hace saltar 5 toast al iniciar la aplicación, con un postDelayed le indicas que salga cada 3 segundos
// (3000millis) y luego con un for hasta 5 veces para que acabe.
public class ServicioAndroid extends Service {

    private Handler h;
    private int i = 1;
    @Override
    public void onCreate() {
        super.onCreate();
        h = new Handler();
    }

    @Override
    public void onDestroy() {
        h.removeCallbacks(ToastTask);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        h.postDelayed(ToastTask, 3000);
        super.onStartCommand(intent, flags, startId);
        return Service.START_STICKY;
    }

    private Runnable ToastTask = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(ServicioAndroid.this, "INICIANDOSE EL JUEGO" + i , Toast.LENGTH_SHORT).show();
            i++;
            if (i > 5) {
                stopSelf();
            }
            h.postDelayed(this, 3000);
        }
    };
}