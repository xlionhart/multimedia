package com.juegoandroid;

import android.app.Activity;
//import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Principal extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent e = new Intent(this,ServicioAndroid.class);
        this.startService(e);
    }


    public void onClick(View view) {
        Intent i = new Intent(this,AndroidLauncher.class);
        startActivity(i);

    }
}
