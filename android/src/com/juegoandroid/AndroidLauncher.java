package com.juegoandroid;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.juegoandroid.main.Game;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new Game(), config);

	}


	//Código que activa el diálogo al dale al botón Back de android, dandote a elegir si quieres
	//salir o no
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exitByBackKey();



			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	protected void exitByBackKey() {

		AlertDialog alertbox = new AlertDialog.Builder(this)
				.setMessage("Do you want to exit application?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {


					public void onClick(DialogInterface arg0, int arg1) {

						finish();
						//close();


					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {


					public void onClick(DialogInterface arg0, int arg1) {
					}
				})
				.show();

	}


}
