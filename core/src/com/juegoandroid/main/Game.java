package com.juegoandroid.main;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.juegoandroid.handlers.BoundedCamera;
import com.juegoandroid.handlers.Content;
import com.juegoandroid.handlers.GameStateManager;

import com.juegoandroid.handlers.MyInput;
import com.juegoandroid.handlers.MyInputProcessor;


//Clase principal del juego donde te crea la pantalla, con sus efectos.
public class Game implements ApplicationListener {

    public static final String TITLE = "Block Bunny";
    public static final int V_WIDTH = 320;
    public static final int V_HEIGHT = 240;
    public static final int SCALE = 2;
    public static final float STEP = 1 / 60f;

    private SpriteBatch sb;
    private BoundedCamera cam;
    private OrthographicCamera hudCam;

    private GameStateManager gsm;

    public static Content res;

    public void create() {

        Gdx.input.setInputProcessor(new MyInputProcessor());

        res = new Content();
        res.loadTexture("images/menu.png");
        res.loadTexture("images/bgs.png");
        res.loadTexture("images/hud.png");
        res.loadTexture("images/bunny.png");
        res.loadTexture("images/crystal.png");
        res.loadTexture("images/spikes.png");

        res.loadSound("sfx/jump.wav");
        res.loadSound("sfx/crystal.wav");
        res.loadSound("sfx/levelselect.wav");
        res.loadSound("sfx/hit.wav");
        res.loadSound("sfx/changeblock.wav");

        res.loadMusic("music/bbsong.ogg");
        res.getMusic("bbsong").setLooping(true);
        res.getMusic("bbsong").setVolume(0.5f);
        res.getMusic("bbsong").play();

        cam = new BoundedCamera();
        cam.setToOrtho(false, V_WIDTH, V_HEIGHT);
        hudCam = new OrthographicCamera();
        hudCam.setToOrtho(false, V_WIDTH, V_HEIGHT);

        sb = new SpriteBatch();

        gsm = new GameStateManager(this);

    }

    public void render() {

        Gdx.graphics.setTitle(TITLE + " -- FPS: " + Gdx.graphics.getFramesPerSecond());

        gsm.update(Gdx.graphics.getDeltaTime());
        gsm.render();
        MyInput.update();

    }

    public void dispose() {
        res.removeAll();
    }

    public void resize(int w, int h) {}

    public void pause() {}

    public void resume() {}

    public SpriteBatch getSpriteBatch() { return sb; }
    public BoundedCamera getCamera() { return cam; }
    public OrthographicCamera getHUDCamera() { return hudCam; }

}