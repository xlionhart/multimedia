package com.juegoandroid.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.juegoandroid.main.Game;
import com.juegoandroid.states.Play;

import java.util.Timer;
import java.util.TimerTask;


// La clase del player la cual cambié entera ya que tuve que introducirle un Time para la base de datos
// y la recogida de cristales, el Time lo hice esta última semana de clase con un código de StackOverflow
// gracias ( a la página ) por existir, y no vi que le incluía un Delay y por eso me tardaba en arrancar.
public class Player extends B2DSprite {

    private int numCrystals;
    private int totalCrystals;
    private int time;
    int delay = 0;
    int period = 1000;


    public Player(Body body) {

        super(body);

        Texture tex = Game.res.getTexture("bunny");
        TextureRegion[] sprites = new TextureRegion[4];
        for(int i = 0; i < sprites.length; i++) {
            sprites[i] = new TextureRegion(tex, i * 32, 0, 32, 32);
        }

        animation.setFrames(sprites, 1 / 12f);

        width = sprites[0].getRegionWidth();
        height = sprites[0].getRegionHeight();

    }

    public void collectCrystal() { numCrystals++; }
    public int getNumCrystals() { return numCrystals; }
    public void setTime(){
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask()
        {
            public void run()
            {


              time++;


            }
        }, delay, period);
       }
    public int getTime(){return  time;}

    public void setTotalCrystals(int i) { totalCrystals = i; }
    public int getTotalCrystals() { return totalCrystals; }

}
