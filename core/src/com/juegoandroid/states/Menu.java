package com.juegoandroid.states;

import static com.juegoandroid.handlers.B2DVars.PPM;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import com.badlogic.gdx.utils.Array;
import com.juegoandroid.entities.B2DSprite;
import com.juegoandroid.handlers.Animation;
import com.juegoandroid.handlers.Background;
import com.juegoandroid.handlers.GameButton;
import com.juegoandroid.handlers.GameStateManager;
import com.juegoandroid.main.Game;





//Clase muy simple que tuve que hacer para el Menu

public class Menu extends GameState {

    private boolean debug = false;

    private Background bg;
    private Animation animation;
    private GameButton playButton;

    private World world;
    private Box2DDebugRenderer b2dRenderer;

    private Array<B2DSprite> blocks;

    public Menu(GameStateManager gsm) {

        super(gsm);

        Texture tex = Game.res.getTexture("menu");
        bg = new Background(new TextureRegion(tex), cam, 1f);


        tex = Game.res.getTexture("hud");
        playButton = new GameButton(new TextureRegion(tex, 0, 34, 58, 27), 160, 100, cam);

        cam.setToOrtho(false, Game.V_WIDTH, Game.V_HEIGHT);


        

    }



    public void handleInput() {

        // mouse/touch input
        if(playButton.isClicked()) {
            Game.res.getSound("crystal").play();
            gsm.setState(GameStateManager.LEVEL_SELECT);
        }

    }

    public void update(float dt) {

        handleInput();



        bg.update(dt);

        playButton.update(dt);

    }

    public void render() {

        sb.setProjectionMatrix(cam.combined);

        // draw background
        bg.render(sb);

        // draw button
        playButton.render(sb);



    }

    public void dispose() {

    }

}