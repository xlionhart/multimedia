package com.juegoandroid.states;
import static  com.juegoandroid.handlers.B2DVars.PPM;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.juegoandroid.entities.HUD;
import com.juegoandroid.entities.Monedas;
import com.juegoandroid.entities.Player;
import com.juegoandroid.entities.Spike;
import com.juegoandroid.handlers.Background;
import com.juegoandroid.handlers.BoundedCamera;

import com.juegoandroid.handlers.MyInput;
import com.juegoandroid.main.Game;
import com.juegoandroid.handlers.B2DVars;
import com.juegoandroid.handlers.GameStateManager;
import com.juegoandroid.handlers.MyContactListener;




//Y se viene la clase más extensa pero no la más dificil, esta clase está modificada entera por mi
//primero llamo a los métodos para formar el mundo, también le pasas la gravedad por parámetro,
//luego viene el metodo de Crear jugador, es muy simple, la única novedad es que le metemos velocidad
//lineal, que hace que el conejo ande solo pero es como un impulso, si te chocas no volvería a impulsarse
// eso me hizo muchos bugs y tuve que ponerle que si la velocidad era nula pues playerDead, luego el createWalls
// que tiene también algo interesante a la hora de cargar los TileMap y es que le introduces en el load un parámetro
// estático LEVEL para que te cargue el nivel que pulsas simplemente llamando a los tmx igual y cambiarle el
//numero, luego el createCrystals, lo hice entero en clase, y me ayudaste con el MapObject porque no me lo
// detectava hasta que copié lo de tus apuntes y ya me funcionó, el createBlocks que esta clase si tiene el mismo
// código que lo saqué de youtube pero no tiene nada de especial, luego el salto del personaje, muy facil
// de hacer, el SwitchBlocks que también es una clase que explica el chico de youtube pero la hice yo porque me
//parecía interesante, también es sencillita, en el HandleInput le metemos el código que hice para reconocer
//la mitad de la pantalla y que si el player no esta tocando el suelo que no puedas spamear el salto.
//En el update pues ya que es lo que se va generando todo el rato por asi decirlo, pues le decimos que si
//el player supera el TileMap pues vuelves al levelselect y te habrías pasado la pantalla, el chico le metía
// que desbloqueabas los niveles conforme te pasabas el anterior pero yo no he podido, mas que nada porque no
// tengo tiempo con tantos trabajos y tal pero seguramente lo podría sacar, y por otra parte la muerte del
//player, ya sea si se cae (eje Y) o si se para su velocidad, y para acabar en el render le hacemos que la
// camara siga el jugador.

public class Play extends GameState {

    private boolean debug = false;

    private World world;
    private Box2DDebugRenderer b2dRenderer;
    private MyContactListener cl;
    private BoundedCamera b2dCam;

    private Player player;

    private TiledMap tileMap;
    private int tileMapWidth;
    private int tileMapHeight;
    private int tileSize;

    private OrthogonalTiledMapRenderer tmRenderer;

    private Array<Monedas> crystals;
    private Array<Spike> spikes;

    private Background[] backgrounds;
    private HUD hud;

    public static int level;

    public Play(GameStateManager gsm) {

        super(gsm);

        // setup del mundo
        world = new World(new Vector2(0, -7f), true);
        cl = new MyContactListener();
        world.setContactListener(cl);
        b2dRenderer = new Box2DDebugRenderer();
        /////////////////////

        createPlayer();

        /////////////////////


        createWalls();
        cam.setBounds(0, tileMapWidth * tileSize, 0, tileMapHeight * tileSize);

        /////////////////////

        createCrystals();
        player.setTotalCrystals(crystals.size);
        player.setTime();

        /////////////////////

        Texture bgs = Game.res.getTexture("bgs");
        TextureRegion sky = new TextureRegion(bgs, 0, 0, 320, 240);
        TextureRegion clouds = new TextureRegion(bgs, 0, 240, 320, 240);
        TextureRegion mountains = new TextureRegion(bgs, 0, 480, 320, 240);
        backgrounds = new Background[3];
        backgrounds[0] = new Background(sky, cam, 0f);
        backgrounds[1] = new Background(clouds, cam, 0.1f);
        backgrounds[2] = new Background(mountains, cam, 0.2f);

        /////////////////////

        hud = new HUD(player,sb);

        b2dCam = new BoundedCamera();
        b2dCam.setToOrtho(false, Game.V_WIDTH / PPM, Game.V_HEIGHT / PPM);
        b2dCam.setBounds(0, (tileMapWidth * tileSize) / PPM, 0, (tileMapHeight * tileSize) / PPM);

    }


    private void createPlayer() {

        // create bodydef
        BodyDef bdef = new BodyDef();
        bdef.type = BodyDef.BodyType.DynamicBody;
        bdef.position.set(60 / PPM, 120 / PPM);
        bdef.fixedRotation = true;
        bdef.linearVelocity.set(1f, 0f);


        Body body = world.createBody(bdef);


        PolygonShape shape = new PolygonShape();
        shape.setAsBox(13 / PPM, 13 / PPM);


        FixtureDef fdef = new FixtureDef();
        fdef.shape = shape;
        fdef.density = 1;
        fdef.friction = 0;
        fdef.filter.categoryBits = B2DVars.BIT_PLAYER;
        fdef.filter.maskBits = B2DVars.BIT_RED_BLOCK | B2DVars.BIT_CRYSTAL | B2DVars.BIT_SPIKE;


        body.createFixture(fdef);
        shape.dispose();


        shape = new PolygonShape();
        shape.setAsBox(13 / PPM, 3 / PPM, new Vector2(0, -13 / PPM), 0);


        fdef.shape = shape;
        fdef.isSensor = true;
        fdef.filter.categoryBits = B2DVars.BIT_PLAYER;
        fdef.filter.maskBits = B2DVars.BIT_RED_BLOCK;


        body.createFixture(fdef).setUserData("foot");;
        shape.dispose();


        player = new Player(body);
        body.setUserData(player);


        MassData md = body.getMassData();
        md.mass = 1;
        body.setMassData(md);



    }


    private void createWalls() {

        tileMap = new TmxMapLoader().load("maps/level" + level + ".tmx");
        tileMapWidth = tileMap.getProperties().get("width", Integer.class);
        tileMapHeight = tileMap.getProperties().get("height", Integer.class);
        tileSize = tileMap.getProperties().get("tilewidth", Integer.class);
        tmRenderer = new OrthogonalTiledMapRenderer(tileMap);


        TiledMapTileLayer layer;
        layer = (TiledMapTileLayer) tileMap.getLayers().get("red");
        createBlocks(layer, B2DVars.BIT_RED_BLOCK);
        layer = (TiledMapTileLayer) tileMap.getLayers().get("green");
        createBlocks(layer, B2DVars.BIT_GREEN_BLOCK);
        layer = (TiledMapTileLayer) tileMap.getLayers().get("blue");
        createBlocks(layer, B2DVars.BIT_BLUE_BLOCK);

    }


    private void createBlocks(TiledMapTileLayer layer, short bits) {


        float ts = layer.getTileWidth();

        BodyDef bdef = new BodyDef();
        bdef.type = BodyDef.BodyType.StaticBody;
        ChainShape cs = new ChainShape();
        Vector2[] v = new Vector2[3];
        v[0] = new Vector2(-ts / 2 / PPM, -ts / 2 / PPM);
        v[1] = new Vector2(-ts / 2 / PPM, ts / 2 / PPM);
        v[2] = new Vector2(ts / 2 / PPM, ts / 2 / PPM);
        cs.createChain(v);
        FixtureDef fd = new FixtureDef();
        fd.friction = 0;
        fd.shape = cs;
        fd.filter.categoryBits = bits;
        fd.filter.maskBits = B2DVars.BIT_PLAYER;


        for(int row = 0; row < layer.getHeight(); row++) {
            for(int col = 0; col < layer.getWidth(); col++) {


                TiledMapTileLayer.Cell cell = layer.getCell(col, row);


                if(cell == null) continue;
                if(cell.getTile() == null) continue;


                bdef.position.set((col + 0.5f) * ts / PPM, (row + 0.5f) * ts / PPM);
                world.createBody(bdef).createFixture(fd);

            }
        }

        cs.dispose();

    }

    private void createCrystals() {


        crystals = new Array<Monedas>();


        MapLayer ml = tileMap.getLayers().get("crystals");
        if(ml == null) return;

        BodyDef bdef = new BodyDef();
        bdef.type = BodyDef.BodyType.StaticBody;
        CircleShape shape = new CircleShape();
        shape.setRadius(8 / PPM);
        FixtureDef fdef = new FixtureDef();
        fdef.shape = shape;
        fdef.isSensor = true;
        fdef.filter.categoryBits = B2DVars.BIT_CRYSTAL;
        fdef.filter.maskBits = B2DVars.BIT_PLAYER;

        for(MapObject mo : ml.getObjects()) {
            float x = 0;
            float y = 0;
            if(mo instanceof EllipseMapObject) {
                EllipseMapObject emo = (EllipseMapObject) mo;
                x = emo.getEllipse().x / PPM;
                y = emo.getEllipse().y / PPM;
            }
            bdef.position.set(x, y);
            Body body = world.createBody(bdef);
            body.createFixture(fdef).setUserData("crystal");
            Monedas c = new Monedas(body);
            body.setUserData(c);
            crystals.add(c);
        }

        shape.dispose();

    }






    private void playerJump() {
        if(cl.playerCanJump()) {
            player.getBody().setLinearVelocity(player.getBody().getLinearVelocity().x, 0);
            player.getBody().applyForceToCenter(0, 200, true);
            Game.res.getSound("jump").play();
        }
    }


    private void switchBlocks() {


        Filter filter = player.getBody().getFixtureList().get(1).getFilterData();
        short bits = filter.maskBits;


        if(bits == B2DVars.BIT_RED_BLOCK) {
            bits = B2DVars.BIT_GREEN_BLOCK;
        }
        else if(bits == B2DVars.BIT_GREEN_BLOCK) {
            bits = B2DVars.BIT_BLUE_BLOCK;
        }
        else if(bits == B2DVars.BIT_BLUE_BLOCK) {
            bits = B2DVars.BIT_RED_BLOCK;
        }


        filter.maskBits = bits;
        player.getBody().getFixtureList().get(1).setFilterData(filter);


        bits |= B2DVars.BIT_CRYSTAL | B2DVars.BIT_SPIKE;
        filter.maskBits = bits;
        player.getBody().getFixtureList().get(0).setFilterData(filter);


        Game.res.getSound("changeblock").play();

    }

    public void handleInput() {


        if(MyInput.isPressed(MyInput.BUTTON1)) {
            playerJump();
        }
        if(MyInput.isPressed(MyInput.BUTTON2)) {
            switchBlocks();
        }


        if(MyInput.isPressed()) {
            if(MyInput.x < Gdx.graphics.getWidth() / 2) {
                switchBlocks();
            }
            else {
                playerJump();
            }
        }

    }

    public void update(float dt) {


        handleInput();




        world.step(Game.STEP, 1, 1);


        Array<Body> bodies = cl.getBodies();
        for(int i = 0; i < bodies.size; i++) {
            Body b = bodies.get(i);
            crystals.removeValue((Monedas) b.getUserData(), true);
            world.destroyBody(bodies.get(i));
            player.collectCrystal();
            Game.res.getSound("crystal").play();
        }
        bodies.clear();


        player.update(dt);

        // La victoria del conejito
        if(player.getBody().getPosition().x * PPM > tileMapWidth * tileSize) {
            Game.res.getSound("levelselect").play();
            gsm.setState(GameStateManager.LEVEL_SELECT);
        }

        // La muerte del conejito
        if(player.getBody().getPosition().y < 0) {
            Game.res.getSound("hit").play();
            gsm.setState(GameStateManager.MENU);
        }
        if(player.getBody().getLinearVelocity().x < 0.001f) {
            Game.res.getSound("hit").play();
            gsm.setState(GameStateManager.MENU);
        }
        if(cl.isPlayerDead()) {
            Game.res.getSound("hit").play();
            gsm.setState(GameStateManager.MENU);
        }


        //cristales
        for(int i = 0; i < crystals.size; i++) {
            crystals.get(i).update(dt);
        }


    }

    public void render() {

        // Camara follow
        cam.setPosition(player.getPosition().x * PPM + Game.V_WIDTH / 4, Game.V_HEIGHT / 2);
        cam.update();


        sb.setProjectionMatrix(hudCam.combined);
        for(int i = 0; i < backgrounds.length; i++) {
            backgrounds[i].render(sb);
        }


        tmRenderer.setView(cam);
        tmRenderer.render();


        sb.setProjectionMatrix(cam.combined);
        player.render(sb);


        for(int i = 0; i < crystals.size; i++) {
            crystals.get(i).render(sb);
        }




        sb.setProjectionMatrix(hudCam.combined);
        hud.render(sb);


        if(debug) {
            b2dCam.setPosition(player.getPosition().x + Game.V_WIDTH / 4 / PPM, Game.V_HEIGHT / 2 / PPM);
            b2dCam.update();
            b2dRenderer.render(world, b2dCam.combined);
        }

    }

    public void dispose() {

    }

}

